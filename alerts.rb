#!/usr/bin/ruby
require 'net/imap'
require 'yaml'

$conf = YAML.load_file "#{File.expand_path(File.dirname($0))}/config.yaml"

def show_messages(messages)
  messages.each do |message|
    `zenity --warning --title='New monitoring events' --text=\'#{message}\' &`
  end
end

def connect
  $imap = Net::IMAP.new($conf[:server], 993, true)
  begin
    $imap.login $conf[:username], $conf[:password]
    $imap.select $conf[:folder]
  rescue => e
    show_messages ["Mailbox connection error: #{e.message}"]
    exit 1
  end
end

connect
show_messages ["Monitoring checking script is up and running!"]

loop do
  begin
    all_messages = []
    not_seen = $imap.search(["NOT", "SEEN"])
    if not_seen != []
      not_seen.each do |message_id|
        subject = $imap.fetch(message_id, "ENVELOPE").first.attr["ENVELOPE"].subject
        subject.gsub!("<", "&lt;")
        subject.gsub!(">", "&gt;")

        # Skip messages from recovered hosts
        if subject.match(/\ OK\ /).nil?
          all_messages.push subject
        end

        # Mark message as readed.
        $imap.store(message_id, "+FLAGS", [:Seen])
      end
    end

    # Show events
    if all_messages.length > $conf[:max_events]
      `zenity --question --title='A lot of messages to show' --text=\'There are #{all_messages.length} new messages found since last check.\\n Do you want to see them all?\'`
      if $?.exitstatus == 0
        show_messages all_messages
      end
    elsif all_messages != []
      show_messages all_messages
    end

    sleep $conf[:check_time]


  # Error checks
  # If I removed messages from monitoring
  rescue Net::IMAP::NoResponseError => e
    puts "ERROR: #{e.message}"
    puts "Monitoring messages was probably deleted"
  # If it was timeout connecting to $imap
  rescue IOError, Errno::EPIPE => e
    puts "ERROR: #{e.message}"
    if $imap.disconnected?
      puts "$imap was disconnected"
      puts "Reconnecting to #{$conf[:server]}"
      connect
    else
      puts "$imap looks like connected, but something was wrong"
    end
  # Otherwise put error message and class end exit
  rescue => e
    puts "ERROR: #{e.message}"
    puts "ERROR CLASS: #{e.class}"
    puts "ERROR TRACE: #{e.backtrace}"
    exit 1
  end
end