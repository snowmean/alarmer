# README #

Simple script for checking certain IMAP folder, and rise alerts if there are new messages using zenity tool (Linux\UNIX only)

Initially was made to check monitoring folder, using outlook.office365.com server, but should works with any IMAP server and any IMAP folder

## Configuration ##
All configurations are in the config.yaml file.

## Dependensies ##
All you need to run the script is ruby and zenity tool installed
